package sample;

import javafx.application.Platform;
import javafx.geometry.Point2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.util.Duration;
import sample.Snake;

public class Apple extends Bonus {

    static final double probability = 1;

    public Apple(Background background) {
        super();
        img = new Image(getClass().getResource("Яблоко.png").toString(), width, height, false, false);
        imageBonus = new ImageView(img);

        this.setTranslateX(startPosition.getX());
        this.setTranslateY(startPosition.getY());


        this.getChildren().add(imageBonus);
    }

    @Override
    protected void action(Snake snake) {

    }

    @Override
    protected Point2D randomizePosition(Platform platform) {
        return null;
    }

    @Override
    protected void remove() {

    }
}
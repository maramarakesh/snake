package sample;

import javafx.application.Platform;
import javafx.geometry.Point2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

public abstract class Bonus extends Pane {

    protected int width;
    protected int height;
    protected Point2D startPosition;

    protected Image img;
    protected ImageView imageBonus;

    protected abstract void action(Snake snake);
    protected abstract Point2D randomizePosition(Platform platform);
    protected abstract void remove();

}
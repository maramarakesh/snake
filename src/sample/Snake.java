package sample;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import com.sun.javafx.scene.traversal.Direction;
import javafx.geometry.Point2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;

import static com.sun.javafx.scene.traversal.Direction.*;
import static sun.nio.ch.IOUtil.load;



public class Snake extends Pane implements Serializable {

    private int width = 30;
    private int height = 30;
    private transient Point2D startPoint = new Point2D(Main.WIDTH / 2 - width / 2,
            Main.HEIGHT - height);
    public transient Point2D velocity;
    private transient Direction direction;
    private final double speed = 1;

    private static final long serialVersionUID = 1L;

    private transient Image img = new Image(getClass().getResource("snake.png").toString(), width, height, false, false);
    private transient Image imgFalling = new Image(getClass().getResource("snake.png").toString(), width, height, false, false);
    private transient ImageView snakeImage;

    public void save() throws IOException {
        ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream("snake.txt"));
        outputStream.writeObject(this);
        outputStream.flush();
        outputStream.close();
    }

    public Snake() throws FileNotFoundException, ClassNotFoundException, IOException {

        snakeImage = new ImageView(img);
        load();

        setTranslateX(startPoint.getX());
        setTranslateY(startPoint.getY());
        direction = Direction.UP;
        velocity = new Point2D(0, 0);
        this.getChildren().add(snakeImage);
    }

    public void update() {
        if (Main.isPressed(KeyCode.RIGHT)) { this.direction = Direction.RIGHT; }
        if (Main.isPressed(KeyCode.DOWN)) { direction = Direction.DOWN;  }
        if (Main.isPressed(KeyCode.UP)) { direction = Direction.UP;  }
        if (Main.isPressed(KeyCode.LEFT)) { this.direction = Direction.LEFT; }

        if (direction == Direction.RIGHT || direction == Direction.LEFT) {
            moveX(this.direction);
        }
        if (direction == Direction.UP || direction == Direction.DOWN) {
            moveY(this.direction);
        }
    }

    public void moveX(Direction direction) {
        for (double i = 0; i < speed; i++) {
            if (direction == Direction.RIGHT) {
                setTranslateX(getTranslateX() + 1);
                if (getTranslateX() + height > Main.WIDTH) {
                    setTranslateX(0);
                }
            }
            if (direction == Direction.LEFT) {
                setTranslateX(getTranslateX() - 1);
                if (getTranslateX() < 0) {
                    setTranslateX(Main.WIDTH);
                }
            }
        }
    }

    public void moveY(Direction direction) {
        for (int i = 0; i < speed; i++) {
            if (direction == Direction.DOWN) {
                setTranslateY(getTranslateY() + 1);
                if (getTranslateY() > Main.HEIGHT) {
                    setTranslateY(0);
                }
            } else if (direction == Direction.UP) {
                setTranslateY(getTranslateY() - 1);
                if (getTranslateY() < 0) {
                    setTranslateY(Main.HEIGHT);
                }
            }
        }
    }


    public void changeDirection(Direction direction) {
        this.direction = direction;
    }
}
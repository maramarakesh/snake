package sample;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

// The main class
public class Main extends Application {

    // Panels
    static Pane appRoot;
    static Pane gameRoot;

    // Player and platforms
    Snake snake;

    // Pressed keys
    public static HashMap <KeyCode, Boolean> keys = new HashMap<>();

    // Window's sizes and background
    public static final int WIDTH = 640;
    public static final int HEIGHT = 480;
    private final Image imgBackground = new Image(getClass().getResource("background.png").toString(),
            WIDTH, HEIGHT, false, false);
    private ImageView background;
    AnimationTimer timer;

    public static boolean isPressed(KeyCode key) {
        return keys.getOrDefault(key, false);
    }

    private Parent createContent() throws FileNotFoundException, ClassNotFoundException, IOException {
        appRoot = new Pane();
        gameRoot = new Pane();
        gameRoot.setPrefSize(WIDTH, HEIGHT);

        snake = new Snake();
        gameRoot.getChildren().add(snake);

        timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                snake.update();
            }
        };

        background = new ImageView(imgBackground);

        appRoot.getChildren().addAll(background, gameRoot);

        return appRoot;
    }

    @Override
    public void start(Stage stage) throws Exception {

        Scene scene = new Scene(createContent());
        scene.setOnKeyPressed(key-> { keys.put(key.getCode(), true); });

        timer.start();

        // Closing window
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                try {
                    snake.save();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                stage.close();
            }
        });

        stage.setTitle("Snake");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}